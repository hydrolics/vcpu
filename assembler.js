//var result = 0x401;
var result = 0x1C05;
//var string = "ADD A, B";
var string = "LOADB A, 0x5";

var assemble = function(string) {
  var opLookup = {
    nop: 0x0,
    add: 0x1,
    sub: 0x2
  };
  
  var regLookup = {
    a: 0x0,
    b: 0x1,
    c: 0x2,
    d: 0x3  
  };
  
  var opcode = string.split(" ", 1);
  var opcode = opcode[0].toLowerCase();
  var operands = string.slice(opcode.length);
  var operands = operands.split(",");
  
  // Trim whitespace
  for (var i=0, length=operands.length; i<length; i++) {
    operands[i] = operands[i].trim();
  }
  
  // [Opcode][SP][SRC][DST]
  // ADD, SUB, MUL DIV
  if (opLookup[opcode] <= 0x4) {
    var instruction = (opLookup[opcode] << 2) | 0x00;                         // SP
    instruction = (instruction << 4) | regLookup[operands[0].toLowerCase()];  // SRC
    instruction = (instruction << 4) | regLookup[operands[1].toLowerCase()];  // DST
  }
  
  // [Opcode][SP][Byte]
  // LOADB
  if (opLookup[opcode] = 0x7) {
    var instruction = (opLookup[opcode] << 2) | regLookup[operands[0].toLowerCase()]; // SP
    instruction = (instruction << 8) | operands[1];                                   // Byte
  }
  
  
  console.log(instruction);
  console.log(result)
};
