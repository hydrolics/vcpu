var cpu = {
  PC: null,  // Program Counter
  
  // General Purpose Registers
  AX: 0x1,
  BX: 0x2,
  CX: 0x3,
  DX: 0x4,
  
  GPR: [],
    
  CMP: null,  // Comparison Flag
  
  clockspeed: 500,  // Clock speed in milliseconds
  
  /**
  *  Places the CPU in its initial starting state
  */
  init: function() {
    for(var i=0; i<5; i++) {
      cpu.GPR[i] = null;
    }
    cpu.PC = 0x00;
  },
  
  
  start: function() {
    if (cpu.PC === null)
      cpu.init();
      
    setInterval(cpu.tick, cpu.clockspeed);
  },
  
  stop: function() {
  
  },
  
  reset: function() {
  
  },
  
  step: function() {
  
  },
  
  tick: function() {
  
    var data = cpu.fetch(cpu.PC);
    log('CPU', ' - fetched: ' + data);
    
    var decodedInstruction = cpu.decode(data);
    var opcode = decodedInstruction[0];
    var operands = decodedInstruction[1];
    log('CPU', ' - decoded: ' + opcode);
    
    cpu.execute(opcode, operands);
    // Increment the Program Counter
    //var nextPC = parseInt(cpu.PC,2);
    cpu.PC++;
  },
  
  /**
  *  Fetch the instruction from memory
  */
  fetch: function(location) {
    return memory.read(location);
  },
  
  /**
  *  Decode the data from the opcode
  */
  decode: function(instruction) {
  
    // Opcode is first 6 bits, so shift right 10, so only the first 6 are remaining
    var opcode = instruction >> 10;
    
    var operands = [];
    var bitMaskOperandSP = 0x300;  // 0000 0011 0000 0000
    var bitMaskOperandSRC = 0xF0;  // 0000 0000 1111 0000
    var bitMaskOperandDST = 0xF;   // 0000 0000 0000 1111
    var bitMaskOperandByte = 0xFF; // 0000 0000 1111 1111
    
    // [Opcode][SP][SRC][DST]
    // ADD, SUB, MUL DIV
    if (opcode <= 0x4) {
      operands[0] = (instruction & bitMaskOperandSP) >> 8; // Special
      operands[1] = (instruction & bitMaskOperandSRC) >> 4; // Source
      operands[2] = instruction & bitMaskOperandDST;
    
    }
    
    // [Opcode][SP][Byte]
    // LOADB
    else if (opcode == 0x7) {
      operands[0] = (instruction & bitMaskOperandSP) >> 8;  // Special
      operands[1] = (instruction & bitMaskOperandByte);   // Source
    }
    
    return [opcode, operands];
  },
  
  /**
  *  Preform the opcode on the data
  */
  execute: function(opcode, operands) {
    switch (opcode) {
      case 0x1:
        cpu.add(operands[0], operands[1], operands[2]);
        break;
      
      case 0x2:
        cpu.sub(operands[0], operands[1], operands[2]);
        break;
        
      //case 0x4:
        //cpu.load(operands[0], operands[1]);
        //break;
        
      case 0x7:
        cpu.load_byte(operands[0], operands[1]);
        break;
        
      default:
        alert('Invalid Opcode');
    }
  },
  
  /**
  *  Adds the Register SRC to the Register DST and places the result in the Register RSLT
  */
  add: function(sp, src, dst) {
    //log('src', cpu.GPR[src]);
    if (sp == 0x0) {
      cpu.GPR[dst] = parseInt(cpu.GPR[src]) + parseInt(cpu.GPR[dst]);
      log('CPU', ' - add: GPR[' + dst + '] := ' + src + ' + ' + dst + ' :: ' + cpu.GPR[dst]);
    }
  },

  /**
  *  Subtracts X from Y, leaving the result in Y
  */  
  sub: function(sp, src, dst) {
    if (sp == 0x0) {
      cpu.GPR[dst] = parseInt(cpu.GPR[src]) - parseInt(cpu.GPR[dst]);
      log('CPU', ' - subtract: GPR[' + dst + '] := ' + src + ' - ' + dst + ' :: ' + cpu.GPR[dst]);
    }  
  },

  load: function(src, dst) {
    log('CPU', ' - load: GPR[' + dst + '] := mem[' + src + ']');
    cpu.GPR[dst] = memory.read(src);

  },
  
  load_byte: function(dst, data) {
    log('CPU', ' - load: GPR[' + dst + '] := ' + data);
    cpu.GPR[dst] = data;
  },


  /**
  *  Moves the contents of X into Y
  */  
  mov: function(x,y) {
  
  },

  /**
  *  Moves the contents of X into the Program Counter, the next instruction will then be executed from the memory location of X
  */
  jmp: function(x) {
  
  },

  // A comparison in a processor can typically be performed by a subtraction operation. If the result is a positive number, the first item is greater than the second item. If the result is a negative number, the first item is less than the first. If the numbers being compared are unsigned, the value of the carry flag will serve the same purpose as the greater-than or less-than flag.
  cmp: function(x,y) {
  
  },

};
