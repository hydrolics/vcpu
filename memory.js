/*var Word = function(value) {
    console.log(value);
  this.value = value;
};

  Word.prototype.set = function(value) {

    this.value = this.toBinary(value);
  };
  
  Word.prototype.toBinary = function(value) {
    return
  };
  
  Word.prototype.wordLength = 16; // Number of bits
  */
  
var log = function(system, msg) {
  var logging = true;
  
  if (logging) {
    console.log('[' + system + ']' + msg);
  }

};

var utils = {

  /**
  *  Converts a value to binary and pads remaining bits
  *  @param value value to convert to binary
  *  @param padding {int} number of bits to pad
  */
  toBinary: function(value, padding) {
    if (padding == null) padding = 16;  // Default to word size of 16bits
    value = value.toString(2);
    
    // Padding
    var tmp = '';
    for(var i=0, length=padding-value.length; i<length; i++) {
      tmp += '0';
    }
    
    return value+tmp;
  }


};

var memory = {
  _mem: [],
  
  /**
  *  @param values {array} Words to populate the memory with
  */
  init: function(values) {
  
    // For each value, create a new word and add it to the memory
    for(var i=0, length=values.length; i < length; i++) {
      memory.write(i, values[i]);
      
      //memory._mem[i] = values[i];
      //console.log("Writing " + values[i] + " to " + i);
    }
  },
  
  /**
  *  Read a value from memory
  *  @param location {Word} binary value of the memory to read from
  */
  read: function(location) {
    // Verify the memory location exists
    if (memory._mem[location] == undefined) {
      console.log('Out of Bounds: ' + memory._mem[location]);
      alert('Memory out of bounds');
    }
    
    else {
      log('MEMORY', '[' + location + '] - read: ' + memory._mem[location]);
      return memory._mem[location];
    }
  },
  
  /**
  *  Write a value to memory
  *  @param location {Word} binary value of the memory to write to
  *  @param value {Word} binary value to write
  */
  write: function(location, value) {
    log('MEMORY', '[' + location + '] - write: ' + value);
    //console.log('Memory[' + location + '] - write: ' + value);
    memory._mem[location] = value;
  },
  
  /**
  *  Returns the size of the memory
  */
  size: function() {
    // FIX: should return in bits
    return memory._mem.length;
  }

};
